#include <stdio.h>
#include <string.h>
#include <time.h>

#include "../headers/headers.h"
#include "../headers/util.h"

long modifyTime = 0;

void add_headers(char *buffer,char *filename) {
  strcat(buffer, "HTTP/1.1 200 OK\r\n");
  if (file_is_modified(filename, modifyTime)) {
    printf("file got modified %s\n", filename);
    modifyTime=time(NULL);
  }
  appendEtagToBuffer(buffer, modifyTime);

  strcat(buffer, "\r\n");
}
void get_method_route(char *methodBuffer, char *routeBuffer,char *message) {
  char *p = message, *m = methodBuffer, *r = routeBuffer;
  while (*p != ' ') {
    *(m++) = *(p++);
  }
  p++;
  while (*p != ' ') {
    *(r++) = *(p++);
  }
}
void appendEtagToBuffer(char *buffer, long etag) {
  char timeStr[50];
  char etagHeader[50] = "Etag: ";
  sprintf(timeStr, "%ld\r\n", etag);
  strcat(etagHeader, timeStr);
  strcat(buffer, etagHeader);
}
