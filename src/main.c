#include "../headers/headers.h"
#include "../headers/http_server.h"
#include "../headers/routes.h"
#include "../headers/util.h"

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <regex.h>
#include <sys/socket.h>
#include <sys/stat.h>

#define MES_LEN 4000

int keepRunning = 1;
void generateLastModifiedHeader(char *header) {
  time_t currentTime = time(NULL);
  struct tm *timeInfo = gmtime(&currentTime);

  strftime(header, 128, "Last-Modified: %a, %d %b %Y %H:%M:%S GMT\r\n",
           timeInfo);
}
void intHandler(int dummy) { keepRunning = 0; }

void loop(HTTP_SERVER *http_server);

int main(int argc, char *argv[]) {
  signal(SIGINT, intHandler);
  int port = 6969;
  if (argc == 2) {
    port = atoi(argv[1]);
  };
  HTTP_SERVER http_server;
  init_server(&http_server, port);
  loop(&http_server);
  stop_server(&http_server);

  printf("stopping\n");
  return 0;
}
void get_first_line(char *from, char *to) {
  char *p = from, *t = to;
  while (*p != '\n') {
    *(t++) = *(p++);
  }
}
char *trim_leading(char *from) {
  char *p = from;
  while (*p == ' ') {
    p++;
  }
  return p;
}
void add_leading_slash(char *buff, int n) {
  char *p = buff;
  char temp1 = *(p);
  char temp2 = *(p + 1);
  while (p < buff + n) {
    *(p + 1) = temp1;
    temp1 = temp2;
    p++;
    temp2 = *(p + 1);
  }
  *buff = '/';
}
void send_html(char *path, char *method, int client_socket) {

  char http_response[40000] = {'\0'};
  add_headers(http_response, path);

  char *response_data = readFile(path);
  if (response_data == NULL) {
    response_data = readFile("./static/404.html");
  }
  if (strcmp(method, "GET") == 0) {
    strcat(http_response, response_data);
  }
  send(client_socket, http_response, sizeof(http_response), 0);
  free(response_data);
}

void send_css(char *path, char *method, int client_socket) {
  char def_path[1000] = "";
  getcwd(def_path, 1000);
  char http_response[40000] = {'\0'};
  add_headers(http_response, path);
  char *response_data = readFile(path);
  strcat(http_response, response_data);
  send(client_socket, http_response, sizeof(http_response), 0);
  free(response_data);
}

void loop(HTTP_SERVER *http_server) {

  while (keepRunning) {
    int client_socket;
    char buffer[MES_LEN + 1] = "";
    client_socket = accept(http_server->socket, NULL, NULL);
    read(client_socket, buffer, MES_LEN);
    regex_t regex_css, regex_html;
    regcomp(&regex_css, "text/css", 0);
    regcomp(&regex_html, "text/html", 0);

    int res_css = regexec(&regex_css, buffer, 0, NULL, 0);
    int res_html = regexec(&regex_html, buffer, 0, NULL, 0);
    char method[100] = "";
    char route[100] = "";
    get_method_route(method, route, buffer);

    char def_path[1000] = "";
    getcwd(def_path, 1000);

    char found_file[1000] = "";
    char path[1000] = "";
    strcat(path, "./static/pages");
    strcat(path, route);
    if (!res_css) {
      send_css(path, method, client_socket);
    } else {
      find_dir_by_path(path, 1, found_file);
      strcpy(found_file, trim_leading(found_file));
      add_leading_slash(found_file, 999);
      strcat(path, found_file);
      chdir(def_path);
      send_html(path, method, client_socket);
    }

    close(client_socket);
  }
}
