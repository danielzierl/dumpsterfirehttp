#include "../headers/routes.h"
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>

void find_dir_by_path(const char *dir, int depth, char *res) {

  DIR *dp;
  struct dirent *entry;
  struct stat statbuf;
  if ((dp = opendir(dir)) == NULL) {


    fprintf(stderr, "cannot open directory: %s\n", dir);
    return;
  }
  chdir(dir);
  while ((entry = readdir(dp)) != NULL) {
    lstat(entry->d_name, &statbuf);
    if (S_ISDIR(statbuf.st_mode)) {
      /* Found a directory, but ignore . and .. */
      if (strcmp(".", entry->d_name) == 0 || strcmp("..", entry->d_name) == 0)
        continue;
      sprintf(res,"%*s%s/", depth, "", entry->d_name);
      /* Recurse at a new indent level */
      find_dir_by_path(entry->d_name, depth + 4, res);
    } else
      sprintf(res,"%*s%s", depth, "", entry->d_name);
  }
  chdir("..");
  closedir(dp);
}
