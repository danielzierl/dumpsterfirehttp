#include "../headers/util.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

char *readFile(char *filename) {
  FILE *f = fopen(filename, "r");
  if (f == NULL) {
    printf("%s does not exist \n", filename);
    return NULL;
  } else {
  }

  fseek(f, 0, SEEK_END);
  long fsize = ftell(f);
  fseek(f, 0, SEEK_SET);
  char *buffer = calloc((fsize+1),sizeof(char));
  char ch;
  int i=0;
  while ((ch = fgetc(f)) != EOF) {
    buffer[i] = ch;
    i++;
  }
  fclose(f);

  return buffer;
}
int file_is_modified(const char *path, time_t oldMTime) {
  struct stat file_stat;
  int err = stat(path, &file_stat);
  if (err != 0) {
    return 1;
  }
  return file_stat.st_mtime > oldMTime;
}
long get_file_time(const char *path) {

  struct stat file_stat;
  int err = stat(path, &file_stat);
  if (err != 0) {
    perror(" [file_is_modified] stat\n");
  }
  return file_stat.st_mtime;
}
