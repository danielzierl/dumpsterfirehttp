#include "../headers/http_server.h"
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
void error(char *msg){
  printf("%s",msg);
  exit(1);
}

void init_server(HTTP_SERVER *server, int port){
  server->port=port;
  int sockfd = socket(AF_INET, SOCK_STREAM, 0);
  server->socket=sockfd;
  struct sockaddr_in address;
  struct sockaddr *addr;
  address.sin_family = AF_INET;
  address.sin_port= htons(port);
  address.sin_addr.s_addr=INADDR_ANY;
  addr = (struct sockaddr*) &address;
  
  printf("socket address %d %s\n", address.sin_addr.s_addr,addr->sa_data);
  printf("listening socket fd %d \n", sockfd);

  int bind_res =bind(sockfd, addr, sizeof(*addr));
  if(bind_res<0)
    error("bind failed\n");
  listen(sockfd, 5);
  printf("HTTP server initialized on port %d\n", port);

}
void stop_server(HTTP_SERVER *server){
  printf("closing sockets\n");
  // close(server->socket);
  shutdown(server->socket, SHUT_RDWR);
}
