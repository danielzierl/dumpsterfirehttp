
typedef struct HTTP_SERVER {
  int port;
  int socket;
} HTTP_SERVER;

void init_server(HTTP_SERVER *server, int port);
void stop_server(HTTP_SERVER *server);
